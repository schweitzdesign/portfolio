module ApplicationHelper
  def public_url(file)
    root_url + file
  end

  def logo_link
    params[:action] == "home" ? '#top' : home_path
  end
end
