class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def pages
  end
end
