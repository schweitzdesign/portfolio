class PagesController < ActionController::Base
  layout 'application'

  def home
  end

  def application_design
  end

  def ecommerce
  end

  def mobile_design
  end

  def responsive_web
  end
end