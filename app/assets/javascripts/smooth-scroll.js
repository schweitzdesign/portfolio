$(document).on('click', '.js-scroll', function (e) {
  e.preventDefault();

  var navtagOffset = 90;

  $('html, body').animate({
    scrollTop: $( $.attr(this, 'href') ).offset().top - navtagOffset
  }, 600);
  return false;
});