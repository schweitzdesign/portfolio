$(window).scroll(function() {
  var scroll = $(window).scrollTop();
  var triggerHeight = 30;

  if (scroll >= triggerHeight) {
    $(".js-logo").addClass("logo-scroll");
  } else {
    $(".js-logo").removeClass("logo-scroll");
  }
});
