// TODO DRY

var scrollFireThreshold = 120;

var options = [
  {selector: '#home_intro', offset: scrollFireThreshold, callback: function() {
    $('#home_intro').addClass('slideUp');
  } },
  {selector: '#growth_hacking', offset: scrollFireThreshold, callback: function() {
    $('#growth_hacking').addClass('slideLeft');
  } },
  {selector: '#application_design', offset: scrollFireThreshold, callback: function() {
    $('#application_design').addClass('slideRight');
  } },
  {selector: '#ecommerce', offset: scrollFireThreshold, callback: function() {
    $('#ecommerce').addClass('slideLeft');
  } },
  {selector: '#mobile', offset: scrollFireThreshold, callback: function() {
    $('#mobile').addClass('slideRight');
  } },
  {selector: '#responsive_web', offset: scrollFireThreshold, callback: function() {
    $('#responsive_web').addClass('slideUp');
  } },
  {selector: '#application_design_intro', offset: scrollFireThreshold, callback: function() {
    $('#application_design_intro').addClass('slideUp');
  } },
  {selector: '#ecommerce_intro', offset: scrollFireThreshold, callback: function() {
    $('#ecommerce_intro').addClass('slideUp');
  } },
  {selector: '#mobile_intro', offset: scrollFireThreshold, callback: function() {
    $('#mobile_intro').addClass('slideUp');
  } },
  {selector: '#responsive_web_intro', offset: scrollFireThreshold, callback: function() {
    $('#responsive_web_intro').addClass('slideUp');
  } },
  {selector: '#growth_hacking_intro', offset: scrollFireThreshold, callback: function() {
    $('#growth_hacking_intro').addClass('slideUp');
  } }
];
Materialize.scrollFire(options);