$(document).ready(navHeight);
$(window).on('resize', function(e) {
  navHeight(e),
  logoTop(e)
});

var logoBlockSize = 80 + 5;

function navHeight() {
  var height = $(window).innerHeight() - logoBlockSize;

  $('.section-nav').css({
    'max-height': height
  });
}


function logoTop() {
  var height = $(window).innerHeight() - logoBlockSize;

  $('.logo.open').css({
    'top': height
  });
}

$('.side-nav').addClass('visible'); // ensure this is not displayed until the page is ready

$('.button-collapse').sideNav({
  onOpen: function() {
    var height = $(window).innerHeight() - logoBlockSize;
    $('#slide-out').addClass('open');
    $('.logo').addClass('open');
    $('.logo').css({
      'top': height
    });
  },
  onClose: function() {
    $('#slide-out').removeClass('open');
    $('.logo').removeClass('open');
    $('.logo').css({
      'top': 0
    });
  },
});

$(".close_nav").click(function() {
  $('.button-collapse').sideNav('hide');
});

