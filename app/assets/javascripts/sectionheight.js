// THIS SCRIPT ONLY HANDLES RESIZE
// FIRST PAINT IS HANDLED IN APPLICATION>ERB
$(window).on('resize', sectionHeight);

function sectionHeight() {
  var height = $(window).innerHeight() * 0.93;
  $('.section').css({
    'min-height': height,
  });
}


