# README

# Homebrew
```
brew update
```

# RVM
```
rvm install 2.2.5
rvm use 2.2.5
```

# Rails (5.1.2)
```
sudo gem install rails
rails new applicationname
```

# Materialize
https://github.com/mkhairi/materialize-sass


# TODO
- Additional CSS optimizations for perfromance (https://github.com/filamentgroup/loadCSS/blob/master/README.md)
- Additional image optimizations for performance (https://github.com/johnkoht/responsive-images)
- Back to Top
- Next section \/ icon
- Jupiter Case Study
- better use of z-index (vars)

#TODONE
- Clean up async and deferred js loading
- Visibility: hidden equivalent for first load of Nav (to prevent flicker)
- Nav Height Bug, + Pace of animate out for side-nav + logo
- Footer note
- Footer text underline style (apply this to all plain text links?)
