Rails.application.routes.draw do
  root 'pages#home'

  controller :pages do
    get :home
    get :application_design
    get :ecommerce
    get :mobile_design
    get :responsive_web
    get :growth_hacking
  end
end
